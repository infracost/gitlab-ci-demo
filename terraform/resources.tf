resource "aws_instance" "web_app" {
  ami           = "ami-674cbc1e"
  instance_type = "m5.large"

  tags = {
    Service = "webapp"
  }

  volume_tags = {
    Service = "webapp"
    Environment = "production"
  }

  root_block_device {
    volume_type = "gp3"
    volume_size = 1000
  }
}

resource "aws_lambda_function" "hello_world" {
  function_name = "hello_world"
  role          = "arn:aws:lambda:us-east-1:account-id:resource-id"
  handler       = "exports.test"
  runtime       = "nodejs12.x"
  memory_size   = 1024

  tags = {
    Service = "api"
  }
}

resource "aws_cloudwatch_log_group" "logs" {
  name = "logs"
  retention_in_days = 365
  tags = {
    Service = "api"
  }
}
