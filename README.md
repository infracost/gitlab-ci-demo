# Infracost GitLab CI Demo

See open and closed [merge requests](https://gitlab.com/infracost/gitlab-ci-demo/-/merge_requests/) for the demo.

The [Infracost GitLab-CI template](https://gitlab.com/infracost/infracost-gitlab-ci/) runs [Infracost](https://infracost.io) against pull requests.

See the [Infracost integrations](https://www.infracost.io/docs/integrations/cicd) page for other integrations.

